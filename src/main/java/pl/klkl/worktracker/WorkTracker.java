
package pl.klkl.worktracker;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class WorkTracker extends Activity {
    private TextView hello;
    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hello = ((TextView) findViewById(R.id.hello));

        mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            //Toast.makeText(getApplicationContext(), "mAdapter is null.", Toast.LENGTH_LONG).show();
        }
       //Toast.makeText(getApplicationContext(), "mAdapter is not null.", Toast.LENGTH_LONG).show();


        mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (!mAdapter.isEnabled()) {
                Toast.makeText(getApplicationContext(), "mAdapter is null in resume part.", Toast.LENGTH_LONG).show();
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater();
        return true;
    }

    @Override
    public void onNewIntent(Intent intent) {
        setIntent(intent);
        //Toast.makeText(getApplicationContext(), "Received intent: " + intent.toString(), Toast.LENGTH_LONG).show();
        String action = intent.getAction();
        analyzeIntent(intent, action);

    }

    private void analyzeIntent(Intent intent, String action) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action) || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action) || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            analyzeDiscoverMessage(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Not Discovered action.", Toast.LENGTH_LONG).show();
        }
    }

    private void analyzeDiscoverMessage(Intent intent) {
        Parcelable[] rawMessage = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

        if (rawMessage != null) {
            NdefMessage[] ndefMessages = getNdefMessages(rawMessage);
            analyzeNDEFMessage(ndefMessages);
        } else {
            Toast.makeText(getApplicationContext(), "Not NDEFMessage tag.", Toast.LENGTH_LONG).show();
        }
    }

    private NdefMessage[] getNdefMessages(Parcelable[] rawMessage) {
        NdefMessage[] ndefMessages = new NdefMessage[rawMessage.length];
        for (int i = 0; i < rawMessage.length; i++) {
            ndefMessages[i] = (NdefMessage) rawMessage[i];
            // Toast.makeText(getApplicationContext(), "ndefMessage: ." + ndefMessages[i].toString(), Toast.LENGTH_LONG).show();
        }
        return ndefMessages;
    }

    private void analyzeNDEFMessage(NdefMessage[] ndefMessages) {
        if (ndefMessages.length != 1) {
            Toast.makeText(getApplicationContext(), "NDEF message lenght not equal 1", Toast.LENGTH_LONG).show();
        }
        NdefMessage message = ndefMessages[0];
        analyzeNDEFRecord(message.getRecords());
    }


    private void analyzeNDEFRecord(NdefRecord[] records) {
        if (records.length < 1) {
            Toast.makeText(getApplicationContext(), "NDEF message lenght less than 1", Toast.LENGTH_LONG).show();
        }
        NdefRecord record = records[0];
        String stringPayload = new String(record.getPayload()).trim();

        if (stringPayload.equals("plnamecollision")) {
            Toast.makeText(getApplicationContext(), "Correct tag. Sending tag information to server...", Toast.LENGTH_LONG).show();
            sentToServer();
        } else {
            Toast.makeText(getApplicationContext(), "Wrong tag information: '" + stringPayload + "'", Toast.LENGTH_LONG).show();
        }

    }

    private void sentToServer() {
        //curl -XPUT http://klkl.pl:8082/workTracker/rest/event/tag

        Client client = Client.create();
        WebResource service = client.resource("http://klkl.pl:8082/workTracker/rest/event/tag");

        ClientResponse response = service.put(ClientResponse.class);

        if ( response.getStatus() == 200)
            Toast.makeText(getApplicationContext(), "Message sent to server with success", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(getApplicationContext(), "Message can't be sent. Server status response:" + response.getStatus(), Toast.LENGTH_LONG).show();


    }
}
